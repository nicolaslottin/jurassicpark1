<?php require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function($string){
        return renderHTMLFromMarkdown($string);
    }));
});


Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function(){
    $data = [
        'dinos' => getDinos(),
    ];
    Flight::render('liste.twig' , $data);
} );

Flight::route('/dinos/@slug/' , function($slug){

    //$res=randomdinos();
    $data = [
        'dinos' => getonedinos($slug) ,
        //'random' => $res ,
    ] ;
    Flight::render('dinos.twig' , $data);
}) ;

Flight::start();


    